﻿using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace CMap
{
    public partial class MainPage : ContentPage
    {
        public Map StockistMap;

        public MainPage()
        {
            InitializeComponent();
            Initialize();
            StockistMap = new Map()
            {
                IsShowingUser = true,
                HeightRequest = 800,
                WidthRequest = 960,
                IsVisible = true,
                MapType = MapType.Street,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            var position1 = new Position(36.8961, 10.1865);
            var position2 = new Position(36.891, 10.181);
            var position3 = new Position(36.892, 10.182);
            var position4 = new Position(36.893, 10.183);
            var position5 = new Position(36.891, 10.185);
            var position6 = new Position(36.892, 10.187);

            var customPin1 = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.Place,
                    Position = position1,
                    Label = "IntilaQ",
                    Address = "Technopark Elgazala, Tunisia"
                },
                URL = "www.intilaq.tn",
            };


            var customPin2 = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.SearchResult,
                    Position = position2,
                    Label = "Telnet R&D",
                    Address = "Technopark Elgazala, Tunisia"
                },
                URL = "www.groupe-telnet.com"
            };

            var customPin3 = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.SearchResult,
                    Position = position3,
                    Label = "Kromberg&Schubert",
                    Address = "Technopark Elgazala, Tunisia"
                },
                URL = "www.kromberg-schubert.com"
            };

            var customPin4 = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.SearchResult,
                    Position = position4,
                    Label = "Via Mobile",
                    Address = "Technopark Elgazala, Tunisia"
                },
                URL = "www.kromberg-schubert.com"
            };

            var customPin5 = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.SearchResult,
                    Position = position5,
                    Label = "Via Mobile",
                    Address = "Technopark Elgazala, Tunisia"
                },
                URL = "www.kromberg-schubert.com"
            };

            var customPin6 = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.SearchResult,
                    Position = position6,
                    Label = "Via Mobile",
                    Address = "Technopark Elgazala, Tunisia"
                },
                URL = "www.kromberg-schubert.com"
            };

            StockistMap.Pins.Add 
            (
                customPin1.Pin
            );
            
            MapContainer.Children.Add(StockistMap);
        }
        private async void Initialize()
        {
            await RetrieveLocation();
        }
        private async Task RetrieveLocation()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));
                StockistMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromKilometers(10)));

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
            }
        }
    }
}
