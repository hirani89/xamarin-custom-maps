﻿using System;
using Xamarin.Forms.Maps;

namespace CMap
{
    public class CustomPin
    {
        public Pin Pin { get; set; }
        public String URL { get; set; }
    }
}
